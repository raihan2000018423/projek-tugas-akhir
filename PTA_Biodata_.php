<!DOCTYPE html>

<head>
    <title> Projek Tugas Akhir </title>
    <link rel="stylesheet" href="PTA.css">
</head>

<body>
    <div class="header">
        <a href="#default" class="logo"><img src="uad3.png" alt="avatar" height="80px" width="80px" id="pic"></a>
        <a><h2>Projek Tugas Akhir</h2></a>
        <a><h2 style="font-size: 25px; font-family: comic sans ms;" id="jam"></h2></a>
        <div class="header-right">
            <br><a href="PTA(Home).php">Home</a>
            <a class="active" href="PTA(Biodata).php">Biodata</a>
            <a href="PTA(Prima).php">Prima</a>
        </div>
    </div>

    <div class="kolom">
    <table>
            <tr><td rowspan="10" width="100px">
            <img src="PP.jpg" alt="avatar" height="250px" width="250px" id="pic">
            <tr><td><b>Nama</b></td><td>:</td> <td>Raihan Fajar Ramadhan</td></tr>
            <tr><td><b>NIM</b></td><td>:</td> <td>2000018423</td></tr>
            <tr><td><b>Alamat</b></td><td>:</td> <td>JL. Bawang Putih 1 No.6 Kota Tangerang Selatan</td></tr>
            <tr><td><b>TTL</b></td><td>:</td> <td>Tangerang, 08 Desember 2001</td></tr>
            <tr><td><b>Email</b></td><td>:</td> <td>rhnfjr@gmail.com</td></tr>
            <tr><td><b>Hobi</b></td><td>:</td> <td>Bermain game</td></tr>
            <tr><td><b>Status</b></td><td>:</td> <td>Mahasiswa</td></tr>
            <table>
            <a><h3>Riwayat Pendidikan : </h3></a>
            <tr><td><b>SD</b></td><td>:</td> <td>SD Negeri 06 Ciputat (2009 - 2014)</td></tr>
            <tr><td><b>SMP</b></td><td>:</td> <td>MTs Negeri 3 Jakarta (2014 - 2017)</td></tr>
            <tr><td><b>SMA</b></td><td>:</td> <td>SMA Negeri 4 Kota Tangerang Selatan (2017 - 2020)</td></tr>
        </table>
    </div>

    <div class="footer"> 
        <font size="3">Copyright &Copy; Raihan Fajar Ramadhan | 2000018423 </font>
    </div>
</body>

<script type="text/javascript">
 window.onload = function() { jam(); }

 function jam() {
  var e = document.getElementById('jam'),
  d = new Date(), h, m, s;
  h = d.getHours();
  m = set(d.getMinutes());
  s = set(d.getSeconds());

  e.innerHTML = h +':'+ m +':'+ s;

  setTimeout('jam()', 1000);
 }

 function set(e) {
  e = e < 10 ? '0'+ e : e;
  return e;
 }
</script>