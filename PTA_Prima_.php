<!DOCTYPE html>

<head>
    <title> Projek Tugas Akhir </title>
    <link rel="stylesheet" href="PTA.css">
</head>

<body>
    <div class="header">
        <a href="#default" class="logo"><img src="uad3.png" alt="avatar" height="80px" width="80px" id="pic"></a>
        <a><h2>Projek Tugas Akhir</h2></a>
        <a><h2 style="font-size: 25px; font-family: comic sans ms;" id="jam"></h2></a>
        <div class="header-right">
            <br><a href="PTA(Home).php">Home</a>
            <a href="PTA(Biodata).php">Biodata</a>
            <a class="active" href="PTA(Prima).php">Prima</a>
        </div>
    </div>

    <div class="kolom">
        <a><h1>Bilangan Prima</h1></a>
        <hr>
        <h3>Inputkan Bilangan</h3>
        <form action="PTA(Prima2).php" method="POST">
        <input type="text" name="prima" placeholder="Masukkan bilangan....">
        <br>
        <input type="submit" class="btn" value="submit">
        </form>
    </div>

    <div class="footer"> 
        <font size="3">Copyright &Copy; Raihan Fajar Ramadhan | 2000018423 </font> 
    </div>
</body>

<script type="text/javascript">
 window.onload = function() { jam(); }

 function jam() {
  var e = document.getElementById('jam'),
  d = new Date(), h, m, s;
  h = d.getHours();
  m = set(d.getMinutes());
  s = set(d.getSeconds());

  e.innerHTML = h +':'+ m +':'+ s;

  setTimeout('jam()', 1000);
 }

 function set(e) {
  e = e < 10 ? '0'+ e : e;
  return e;
 }
</script>