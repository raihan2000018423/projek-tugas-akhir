<!DOCTYPE html>

<head>
    <title> Projek Tugas Akhir </title>
    <link rel="stylesheet" href="PTA.css">
</head>

<body>
    <div class="icon-bar">
        <a href="https://www.facebook.com/profile.php?id=100008564088758" class="facebook"><i class="fa fa-facebook"></i></a> 
        <a href="https://twitter.com/PatarSky" class="twitter"><i class="fa fa-twitter"></i></a> 
        <a href="https://www.youtube.com/channel/UCjCo2yJdaXeuNdvb6zFKfjA" class="youtube"><i class="fa fa-youtube"></i></a> 
    </div>
    <div class="header">
        <a href="#default" class="logo"><img src="uad3.png" alt="avatar" height="80px" width="80px" id="pic"></a>
        <a><h2 >Projek Tugas Akhir</h2></a>
        <a><h2 style="font-size: 25px; font-family: comic sans ms;" id="jam"></h2></a>
        <div class="header-right">
            <br><a class="active" href="PTA(Home).php">Home</a>
            <a href="PTA(Biodata).php">Biodata</a>
            <a href="PTA(Prima).php">Prima</a>
        </div>
    </div>

    <center><h1>Projek Tugas Akhir</h1></center>
    <hr class="new5">

    <div class="kolom">
        Allhamdulilah kita sudah memasuki pertemuan terakhir yaitu pertemuan 13 dan 14 pada mata kuliah pemrograman web ini, 
        Saya sangat berterimakasih kepada Bapak dosen, Bapak Ali Tarmuji yang telah memberikan saya banyak ilmu tentang segala hal,
        terutama tentang pemrograman web ini, saya juga ingin berterimakasih kepada teman - teman semua yang telah berjuang bersama dan 
        banyak juga membantu saya baik dalam mengerjakan tugas dan hal yang lainnya.
        <br><br>
        <center><h2>Segitiga Bintang</h2></center>
        <hr>
        <div id="card">
            Masukkan tinggi bintang
            <form method="POST">
            <input type="text" name="tinggi" placeholder="Masukkan tinggi....">
            </br>
            <input type="submit" name="go" value="SUBMIT">
            </br>
            <?php
		        if (isset($_POST["go"])) {
		        	$nil = $_POST["tinggi"];
			     for($a=1; $a<=$nil; $a++){
                    for($b=$nil; $b>=$a; $b-=1){
                        print('&nbsp');
                }
                    for($c=1; $c<=$a; $c++){
                        echo '*';
                }
                echo "<br/>";
                    }
		        }
	         ?>
        </div>
    </div>

    <div class="footer"> 
        <font size="3">Copyright &Copy; Raihan Fajar Ramadhan | 2000018423 </font>
    </div>
</body>

<script type="text/javascript">
 window.onload = function() { jam(); }

 function jam() {
  var e = document.getElementById('jam'),
  d = new Date(), h, m, s;
  h = d.getHours();
  m = set(d.getMinutes());
  s = set(d.getSeconds());

  e.innerHTML = h +':'+ m +':'+ s;

  setTimeout('jam()', 1000);
 }

 function set(e) {
  e = e < 10 ? '0'+ e : e;
  return e;
 }
</script>